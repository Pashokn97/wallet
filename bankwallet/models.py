from django.contrib.auth.base_user import AbstractBaseUser
from django.db import models

from bankwallet.manager import CustomUserManager


class BankUser(AbstractBaseUser):
    username = models.CharField(max_length=100, unique=True, verbose_name='Имя пользователя')
    password = models.CharField(max_length=100, verbose_name='Пароль')

    USERNAME_FIELD = 'username'
    objects = CustomUserManager()

    class Meta:
        verbose_name = 'Клиент банка'
        verbose_name_plural = 'Клиенты банка'


class Wallet(models.Model):
    balance = models.FloatField(default=0, verbose_name='Баланс в рублях')
    user = models.ForeignKey('BankUser', on_delete=models.CASCADE, verbose_name='Пользователь')

    class Meta:
        verbose_name = 'Кошелек'
        verbose_name_plural = 'Кошельки'


class Operations(models.Model):
    value = models.IntegerField(verbose_name='Сумма транзакции в копейках')
    receiver = models.ForeignKey('BankUser', on_delete=models.CASCADE, related_name='receiver_operations', null=True,
                                 blank=True, verbose_name='Получатель')
    actor = models.ForeignKey('BankUser', on_delete=models.CASCADE, related_name='sender_operations',
                              verbose_name='Актор')
    operation_type = models.CharField(max_length=100, verbose_name='Тип операции')

    class Meta:
        verbose_name = 'Операция клиента'
        verbose_name_plural = 'Операции клиента'


class TransactionType(models.TextChoices):
    receive = 'Получение'
    send = 'Отправление'


class OperationType(models.TextChoices):
    transaction = 'Операция перевода'
    replenishment = 'Операция зачисления'
