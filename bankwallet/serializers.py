from rest_framework import serializers
from bankwallet.models import BankUser, Wallet, Operations, OperationType, TransactionType


class UsersSerializers(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True)
    repeat_password = serializers.CharField(write_only=True)

    def validate(self, attrs):
        if attrs['password'] != attrs['repeat_password']:
            raise serializers.ValidationError('Password mismatch')
        del attrs['repeat_password']

        return attrs

    def create(self, validated_data):
        password = validated_data.pop("password")
        user = BankUser.objects.create(**validated_data)
        user.set_password(password)
        user.save()
        wallet = Wallet.objects.create(balance=0, user=user)
        wallet.save()

        return user

    class Meta:
        model = BankUser
        fields = ('username', 'password', "repeat_password",)


class ReplenishmentSerializer(serializers.ModelSerializer):
    amount = serializers.IntegerField()

    class Meta:
        model = Wallet
        fields = ('amount',)


class TransferSeializer(serializers.Serializer):
    pk = serializers.PrimaryKeyRelatedField(queryset=BankUser.objects.all())
    amount = serializers.IntegerField()


class BalanceSerializer(serializers.ModelSerializer):

    class Meta:
        model = Wallet
        fields = ('balance', )


class OperationsSerializer(serializers.ModelSerializer):
    transaction_type = serializers.SerializerMethodField()

    class Meta:
        model = Operations
        fields = ('receiver', 'value', 'actor', 'operation_type', 'transaction_type')

    def get_transaction_type(self, obj):
        if obj.operation_type == OperationType.replenishment:
            return
        if self.context['request'].user == obj.actor:
            return TransactionType.send
        if self.context['request'].user == obj.receiver:
            return TransactionType.receive


