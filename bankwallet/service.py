from django.db import transaction
from rest_framework.exceptions import ValidationError
from bankwallet.models import Wallet, BankUser, Operations, OperationType


def to_rubles(amount):
    return amount / 100


def replenishment_wallet(amount, pk):
    amount_in_rubles = to_rubles(amount)

    user = BankUser.objects.get(pk=pk)
    wallet = Wallet.objects.select_for_update().get(user=user.pk)
    with transaction.atomic():
        wallet.balance += amount_in_rubles
        wallet.save()

    Operations.objects.create(value=amount, actor=user, operation_type=OperationType.replenishment)
    return wallet.balance


def transfer(sender, receiver, amount):
    amount_in_rubles = to_rubles(amount)

    sender_wallet = Wallet.objects.select_for_update().get(user=sender.pk)
    receiver_wallet = Wallet.objects.select_for_update().get(user=receiver.pk)

    if sender_wallet.balance < amount_in_rubles:
        raise ValidationError('Недостаточно средств')

    with transaction.atomic():
        sender_wallet.balance = sender_wallet.balance - amount_in_rubles
        receiver_wallet.balance += amount_in_rubles
        sender_wallet.save()
        receiver_wallet.save()

    Operations.objects.create(value=amount, receiver=receiver, actor=sender, operation_type=OperationType.transaction)
