from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import generics
from rest_framework.generics import GenericAPIView
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response

from bankwallet.models import BankUser, Wallet, Operations
from bankwallet.serializers import UsersSerializers, ReplenishmentSerializer, TransferSeializer, \
    BalanceSerializer, OperationsSerializer
from bankwallet.service import replenishment_wallet, transfer


class CreateUser(generics.CreateAPIView):
    queryset = BankUser.objects.all()
    serializer_class = UsersSerializers
    permission_classes = (AllowAny,)


class ReplenishmentWallet(GenericAPIView):
    queryset = Wallet.objects.all()
    serializer_class = ReplenishmentSerializer
    permission_classes = (IsAuthenticated, )

    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = self.request.user
        amount = serializer.validated_data['amount']
        actual_balance = replenishment_wallet(amount, user.pk)
        return Response({"balance": actual_balance})


class SendMoney(GenericAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = TransferSeializer

    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        sender = self.request.user
        receiver = serializer.validated_data['pk']
        amount = serializer.validated_data['amount']
        transfer(sender, receiver, amount)
        return Response()


class GetBalance(GenericAPIView):
    permission_classes = (IsAuthenticated, )
    serializer_class = BalanceSerializer

    def get(self, request):
        user = request.user
        wallet = Wallet.objects.get(pk=user.pk)
        return Response({'balance': wallet.balance})


class OperationsList(generics.ListAPIView):
    permission_classes = (IsAuthenticated, )
    serializer_class = OperationsSerializer
    queryset = Operations.objects.all()
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['operation_type']

    def get_queryset(self):
        return self.queryset.filter(actor=self.request.user.pk) | self.queryset.filter(receiver=self.request.user.pk)

